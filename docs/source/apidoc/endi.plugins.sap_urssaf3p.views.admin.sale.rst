endi.plugins.sap\_urssaf3p.views.admin.sale package
===================================================

Submodules
----------

endi.plugins.sap\_urssaf3p.views.admin.sale.tva module
------------------------------------------------------

.. automodule:: endi.plugins.sap_urssaf3p.views.admin.sale.tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.plugins.sap_urssaf3p.views.admin.sale
   :members:
   :undoc-members:
   :show-inheritance:
