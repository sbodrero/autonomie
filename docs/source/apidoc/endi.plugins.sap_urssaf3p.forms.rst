endi.plugins.sap\_urssaf3p.forms package
========================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.plugins.sap_urssaf3p.forms.admin
   endi.plugins.sap_urssaf3p.forms.tasks

Submodules
----------

endi.plugins.sap\_urssaf3p.forms.customer module
------------------------------------------------

.. automodule:: endi.plugins.sap_urssaf3p.forms.customer
   :members:
   :undoc-members:
   :show-inheritance:

endi.plugins.sap\_urssaf3p.forms.tva module
-------------------------------------------

.. automodule:: endi.plugins.sap_urssaf3p.forms.tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.plugins.sap_urssaf3p.forms
   :members:
   :undoc-members:
   :show-inheritance:
