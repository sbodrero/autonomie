endi.panels.supply package
==========================

Submodules
----------

endi.panels.supply.supplier\_invoice\_list module
-------------------------------------------------

.. automodule:: endi.panels.supply.supplier_invoice_list
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.supply.supplier\_order\_list module
-----------------------------------------------

.. automodule:: endi.panels.supply.supplier_order_list
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.panels.supply
   :members:
   :undoc-members:
   :show-inheritance:
