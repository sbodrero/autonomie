endi.views.admin.sale package
=============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.views.admin.sale.accounting
   endi.views.admin.sale.business_cycle
   endi.views.admin.sale.forms
   endi.views.admin.sale.pdf

Submodules
----------

endi.views.admin.sale.catalog module
------------------------------------

.. automodule:: endi.views.admin.sale.catalog
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.admin.sale.index module
----------------------------------

.. automodule:: endi.views.admin.sale.index
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.admin.sale.internal\_invoicing\_numbers module
---------------------------------------------------------

.. automodule:: endi.views.admin.sale.internal_invoicing_numbers
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.admin.sale.numbers module
------------------------------------

.. automodule:: endi.views.admin.sale.numbers
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.admin.sale.receipts module
-------------------------------------

.. automodule:: endi.views.admin.sale.receipts
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.admin.sale.tva module
--------------------------------

.. automodule:: endi.views.admin.sale.tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.views.admin.sale
   :members:
   :undoc-members:
   :show-inheritance:
