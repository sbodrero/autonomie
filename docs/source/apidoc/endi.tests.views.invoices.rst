endi.tests.views.invoices package
=================================

Submodules
----------

endi.tests.views.invoices.conftest module
-----------------------------------------

.. automodule:: endi.tests.views.invoices.conftest
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.invoices.test\_invoice module
----------------------------------------------

.. automodule:: endi.tests.views.invoices.test_invoice
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.invoices.test\_rest\_api module
------------------------------------------------

.. automodule:: endi.tests.views.invoices.test_rest_api
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.views.invoices
   :members:
   :undoc-members:
   :show-inheritance:
