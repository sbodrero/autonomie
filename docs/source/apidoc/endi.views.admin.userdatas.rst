endi.views.admin.userdatas package
==================================

Submodules
----------

endi.views.admin.userdatas.career\_stage module
-----------------------------------------------

.. automodule:: endi.views.admin.userdatas.career_stage
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.admin.userdatas.options module
-----------------------------------------

.. automodule:: endi.views.admin.userdatas.options
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.admin.userdatas.templates module
-------------------------------------------

.. automodule:: endi.views.admin.userdatas.templates
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.views.admin.userdatas
   :members:
   :undoc-members:
   :show-inheritance:
