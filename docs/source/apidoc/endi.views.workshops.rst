endi.views.workshops package
============================

Submodules
----------

endi.views.workshops.export module
----------------------------------

.. automodule:: endi.views.workshops.export
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.workshops.lists module
---------------------------------

.. automodule:: endi.views.workshops.lists
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.workshops.workshop module
------------------------------------

.. automodule:: endi.views.workshops.workshop
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.views.workshops
   :members:
   :undoc-members:
   :show-inheritance:
