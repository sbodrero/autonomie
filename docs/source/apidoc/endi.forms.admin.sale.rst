endi.forms.admin.sale package
=============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.forms.admin.sale.business_cycle

Submodules
----------

endi.forms.admin.sale.bookeeping module
---------------------------------------

.. automodule:: endi.forms.admin.sale.bookeeping
   :members:
   :undoc-members:
   :show-inheritance:

endi.forms.admin.sale.form\_options module
------------------------------------------

.. automodule:: endi.forms.admin.sale.form_options
   :members:
   :undoc-members:
   :show-inheritance:

endi.forms.admin.sale.insurance module
--------------------------------------

.. automodule:: endi.forms.admin.sale.insurance
   :members:
   :undoc-members:
   :show-inheritance:

endi.forms.admin.sale.mentions module
-------------------------------------

.. automodule:: endi.forms.admin.sale.mentions
   :members:
   :undoc-members:
   :show-inheritance:

endi.forms.admin.sale.naming module
-----------------------------------

.. automodule:: endi.forms.admin.sale.naming
   :members:
   :undoc-members:
   :show-inheritance:

endi.forms.admin.sale.tva module
--------------------------------

.. automodule:: endi.forms.admin.sale.tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.forms.admin.sale
   :members:
   :undoc-members:
   :show-inheritance:
