Composants d'enDI
==================

Web server
----------

enDI est basé sur le langage Python et le framework web Pyramid


Database
--------

enDI utilises une base de données Mysql (Mariadb)

Pour y accéder nous utilisons

* SQLAlchemy : ORM for database object representation
* Alembic : Database migration (downgrade/upgrade) (cf :doc:`alembic_migrations`)
* mysql-python

Forms handling
--------------

* Deform : form library
* Deform bootstrap : css/javascript Twitter bootstrap extension of deform
* Colander : form validation tool

Javascript
----------

The following js libraries are used:

* Jquery : DOM manipulation

JS Buildé (composant dynamique de l'interface)
.............................................

* Webpack
* Backbone
* Marionette


Css Stylesheet
--------------

Custom stylesheets are used for the layout. Check the Styleguide for more information.
