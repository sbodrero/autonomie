
module("Date handling");
test("Date To Iso", function(){
  // L'objet Date prend les mois en partant de janvier->0
  pdate = parseDate("2012-12-25");
  edate = new Date(2012, 11, 25);
  equal(pdate.year, edate.year);
  equal(pdate.month, edate.month);
  equal(pdate.day, edate.day);
});
test("Date to Iso", function(){
  // L'objet Date prend les mois en partant de janvier->0
  var date = new Date(2014, 06, 12);
  equal(dateToIso(date), '2014-07-12');
});
test("Date to local format", function(){
  // L'objet Date prend les mois en partant de janvier->0
  var date = new Date(2014, 06, 12);
  equal(dateToLocaleFormat(date), '12/07/2014');
});
