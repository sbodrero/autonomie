import Radio from 'backbone.radio';

import BaseModel from "../../base/models/BaseModel.js";
import DuplicableMixin from '../../base/models/DuplicableMixin.js';

const SupplierInvoiceModel =  BaseModel.extend(DuplicableMixin).extend({
    props: [
        'id',
        'date',
        'remote_invoice_number',
        'supplier_orders',
        'orders_total',
        'orders_cae_total',
        'orders_worker_total',
        'orders_total_ht',
        'orders_total_tva',
        'cae_percentage',
        'customer_id',
        'project_id',
        'business_id',
        'customer_label',
        'project_label',
        'business_label',
        'payments',
        'cae_payments',
        'user_payments',
        'payer_name',
        'payer_id',
        'supplier_name',
        'supplier_id',
    ],
    defaults: {
        supplier_orders: [],
        business_label: '',
        project_label: '',
        customer_label: '',
        remote_invoice_number: '',
    },
    validation: {
        date: {
            required: true,
            msg: 'La date est requise',
        },
        remote_invoice_number: {
            required: true,
            msg: 'Le numéro de facture du fournisseur est requis',
        },
        payer_id : {
            required: function(val, attr, computed) {
                return computed.cae_percentage < 100;
            },
            msg: "Préciser quel entrepreneur réalisera l'avance",
        },
        supplier_id: {
            required: true,
            msg: 'Le fournisseur est requis',
        },
    },
    initialize(){
        SupplierInvoiceModel.__super__.initialize.apply(this, arguments);
        this.on('change:supplier_orders', this.ensureTypesIsList, this);
    },
    ensureTypesIsList(){
        let orders = this.get('supplier_orders');
        if (!_.isArray(orders)){
            this.attributes['supplier_orders'] = [orders];
        }
    },

});
export default SupplierInvoiceModel;
